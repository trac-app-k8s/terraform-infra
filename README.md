# Azure terraform infrastructure

Here stored the code to prepare Azure Cloud for a workload:

- [trac.shatohin.space](http://trac.shatohin.space) - web-based management of software projects

Used terraform providers:

- [`hashicorp/helm`](https://registry.terraform.io/providers/hashicorp/helm/latest/docs)
- [`hashicorp/kubernetes`](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs)
- [`hashicorp/azurerm`](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs)

Actual list of providers stored in [`resources/providers.tf`](./resources/providers.tf)

## Variables

| Name                      | Description                | Default value    |
|---------------------------|----------------------------|------------------|
| `postgres_cluster_name`   | PostgreSQL server name     | `postgresql-01`  |
| `postgres_admin_login`    | PostgreSQL server login    | `""`             |
| `postgres_admin_password` | PostgreSQL server password | `""`             |
| `dns_zone`                | DNS root domain            | `shatohin.space` |
| `trac_admin_password`     | Trac admin password        | `""`             |

## Gitlab Managed Terraform state

Terraform state stored in [Gitlab](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html).

## CI/CD process

Command `terraform plan` integrated in [Gitlab CI](https://docs.gitlab.com/ee/user/infrastructure/iac/mr_integration.html)
for every merge request.

Command `terraform apply` available only on branch `main`
