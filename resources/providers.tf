# A list of providers
terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.7.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.13.1"
    }
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~> 3.31.0"
    }
  }
}

# Configure the Helm Provider
provider "helm" {
  kubernetes {
    host                   = module.aks_cluster.kubeconfig.host
    username               = module.aks_cluster.kubeconfig.username
    password               = module.aks_cluster.kubeconfig.password
    client_certificate     = base64decode(module.aks_cluster.kubeconfig.client_certificate)
    client_key             = base64decode(module.aks_cluster.kubeconfig.client_key)
    cluster_ca_certificate = base64decode(module.aks_cluster.kubeconfig.cluster_ca_certificate)
  }
}

# Configure the Kubernetes Provider
provider "kubernetes" {
  host                   = module.aks_cluster.kubeconfig.host
  username               = module.aks_cluster.kubeconfig.username
  password               = module.aks_cluster.kubeconfig.password
  client_certificate     = base64decode(module.aks_cluster.kubeconfig.client_certificate)
  client_key             = base64decode(module.aks_cluster.kubeconfig.client_key)
  cluster_ca_certificate = base64decode(module.aks_cluster.kubeconfig.cluster_ca_certificate)
}

# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}
}
