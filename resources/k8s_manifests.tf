resource "kubernetes_namespace" "projectcontour" {
  metadata {
    labels = {
      created-by = "terraform"
    }

    name = "projectcontour"
  }
}

resource "kubernetes_namespace" "monitoring" {
  metadata {
    labels = {
      created-by = "terraform"
    }

    name = "monitoring"
  }
}

resource "kubernetes_namespace" "trac" {
  metadata {
    labels = {
      created-by = "terraform"
    }

    name = "trac"
  }
}
