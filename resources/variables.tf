# Postgres module variables
variable "postgres_cluster_name" {
  default = "postgresql-01"
}

variable "postgres_admin_login" {
  sensitive = true
}

variable "postgres_admin_password" {
  sensitive = true
}

# DNS module variables
variable "dns_zone" {
  default = "shatohin.space"
}

# Trac variables
variable "trac_admin_password" {
  sensitive = true
}
