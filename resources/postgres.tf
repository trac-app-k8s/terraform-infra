# Creates PostgreSQL server
module "postgresql_server" {
  source = "git::https://gitlab.com/trac-app-k8s/terraform-modules.git//postgres?ref=v2.0.0"

  name                          = var.postgres_cluster_name
  resource_group_name           = azurerm_resource_group.group.name
  resource_group_location       = azurerm_resource_group.group.location
  storage_mb                    = 6144
  administrator_login           = var.postgres_admin_login
  administrator_password        = var.postgres_admin_password

  databases = {
    trac_db = {
      charset   = "UTF8"
      collation = "English_United States.1252"
    }
  }

  allowed_ips = {
    all = {
      start_ip = "0.0.0.0"
      end_ip   = "0.0.0.0"
    }
  }
}
