# Creates new Public IP for Kubernetes Loadbalancer
resource "azurerm_public_ip" "ip_address" {
  name                = "lb_ip"
  resource_group_name = module.aks_cluster.node_resource_group
  location            = azurerm_resource_group.group.location
  allocation_method   = "Static"
  sku                 = "Standard"
}
