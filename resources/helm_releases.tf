resource "helm_release" "contour" {
  depends_on = [
    kubernetes_namespace.projectcontour,
    module.aks_cluster
  ]

  name      = "contour"
  namespace = "projectcontour"
  version   = "9.2.0"

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "contour"

  values = [
    <<-EOT
      contour:
        service:
          annotations:
            prometheus.io/port: "8000"
            prometheus.io/scrape: "true"

      envoy:
        service:
          annotations:
            prometheus.io/path: /stats/prometheus
            prometheus.io/port: "8002"
            prometheus.io/scrape: "true"

          loadBalancerIP: "${azurerm_public_ip.ip_address.ip_address}"
    EOT
  ]
}

resource "helm_release" "trac" {
  depends_on = [
    kubernetes_namespace.trac,
    module.aks_cluster,
    module.postgresql_server
  ]

  name      = "trac"
  namespace = "trac"
  version   = "1.0.1"

  repository = "https://gitlab.com/api/v4/projects/39956701/packages/helm/stable"
  chart      = "trac"

  values = [
    <<-EOT
      image:
        tag: "1.4.3"

      env:
      - name: TRAC_PROJECT_NAME
        value: Shatohin-Project
      - name: TRAC_CONFIG_HEADER_LOGO__SRC
        value: https://i.ibb.co/hWB9c9P/002-w-95352f5e.jpg
      - name: TRAC_CONFIG_HEADER_LOGO__HEIGHT
        value: "100"
      - name: TRAC_CONFIG_HEADER_LOGO__LINK
        value: "/timeline"

      postgresql:
        enabled: false

      ingress:
        enabled: true
        ingressClassName: "contour"
        hosts:
        - host: trac.${var.dns_zone}
          paths:
          - path: /
            pathType: ImplementationSpecific

      secrets:
        TRAC_ADMIN_PASSWORD: "${var.trac_admin_password}"
        TRAC_DB_STRING: "postgres://${var.postgres_admin_login}%40${var.postgres_cluster_name}:${var.postgres_admin_password}@${module.postgresql_server.fqdn}:5432/trac_db"
    EOT
  ]
}
