module "dns_zone" {
  source = "git::https://gitlab.com/trac-app-k8s/terraform-modules.git//dns?ref=v2.0.0"

  resource_group_name = azurerm_resource_group.group.name

  dns_zone  = var.dns_zone
  a_records = {
    "root" = {
      name = "*"
      ttl  = 600
      ips  = [ azurerm_public_ip.ip_address.ip_address ]
    }
  }
}
