# Creates resource group
resource "azurerm_resource_group" "group" {
  name     = "test-resources"
  location = "West Europe"
}
