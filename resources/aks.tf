# Creates cluster in Azure Kubernetes Service
module "aks_cluster" {
  source = "git::https://gitlab.com/trac-app-k8s/terraform-modules.git//aks?ref=v2.0.0"

  name                    = "cluster-01"
  resource_group_name     = azurerm_resource_group.group.name
  resource_group_location = azurerm_resource_group.group.location
  node_count              = 2
  kubernetes_version      = "1.24"
  node_vm_size            = "standard_b2s"
}
