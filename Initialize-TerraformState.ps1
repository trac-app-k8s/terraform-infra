# Get env from arguments
$env:GITLAB_USERNAME = $args[0]
if (!$args[0]) {
  Write-Host -ForegroundColor Red "❌ You must provide your gitlab username to continue"
  Exit 2
}

$env:GITLAB_URL = "https://gitlab.com"
$env:GITLAB_PROJECT_ID = "39801194"
$env:GITLAB_STATE_NAME = "main"

if (!$env:GITLAB_ACCESS_TOKEN) {
  Write-Host "🔑 Please enter your Gitlab access token:"
  $gitlab_access_token_input = Read-Host -MaskInput
  $env:GITLAB_ACCESS_TOKEN = $gitlab_access_token_input

  if (!$gitlab_access_token_input) {
    Write-Host -ForegroundColor Red "❌ You must provide your gitlab access token to continue"
    Exit 2
  }
}

Write-Host -ForegroundColor Green "✨ Initializing terraform..."

terraform init `
  -backend-config="address=$env:GITLAB_URL/api/v4/projects/$env:GITLAB_PROJECT_ID/terraform/state/$env:GITLAB_STATE_NAME" `
  -backend-config="lock_address=$env:GITLAB_URL/api/v4/projects/$env:GITLAB_PROJECT_ID/terraform/state/$env:GITLAB_STATE_NAME/lock" `
  -backend-config="unlock_address=$env:GITLAB_URL/api/v4/projects/$env:GITLAB_PROJECT_ID/terraform/state/$env:GITLAB_STATE_NAME/lock" `
  -backend-config="username=$env:GITLAB_USERNAME" `
  -backend-config="password=$env:GITLAB_ACCESS_TOKEN" `
  -backend-config="lock_method=POST" `
  -backend-config="unlock_method=DELETE" `
  -backend-config="retry_wait_min=5" # -upgrade
